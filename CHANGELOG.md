## [3.1.0](https://gitlab.com/moshin_han/scada/compare/v3.0.0...v3.1.0) (2020-07-07)


### Features

* add some words again ([8763cc6](https://gitlab.com/moshin_han/scada/commit/8763cc682e1f134e5c3c4e9b2d246efad81c8cdd))

## [3.0.0](https://gitlab.com/moshin_han/scada/compare/v2.0.0...v3.0.0) (2020-07-06)


### ⚠ BREAKING CHANGES

* 路由跳转方法route()传参方式更改

修改前:
route(from, to, ...others)

修改后:
route({
  from,
  to,
  ...others,
})

### Features

* 新增产品管理功能 ([7172918](https://gitlab.com/moshin_han/scada/commit/717291888feb359866c45a0f5c690bbc68fea5a4))
* 设备管理页面查询表单新增"设备类型"作为查询条件 ([9c0f0a3](https://gitlab.com/moshin_han/scada/commit/9c0f0a358b65fc432a6ef62f40afc0ea178058ff))


### Bug Fixes

* 修复产品管理页面“新增“按钮与其它按钮没有对齐的问题 ([e19ef8e](https://gitlab.com/moshin_han/scada/commit/e19ef8eb8e91ec5480b12e3a43d23a7544229645))
* 修复系统管理页面内菜单无法跳转的问题 ([4f50b5a](https://gitlab.com/moshin_han/scada/commit/4f50b5a9e5cb5e71ec6d854917542c1642ab3675))

## [2.0.0](https://gitlab.com/moshin_han/scada/compare/v1.2.0...v2.0.0) (2020-06-24)


### ⚠ BREAKING CHANGES

* api1 and api2

### Features

* add some words with Breaking Changes ([2f0b88c](https://gitlab.com/moshin_han/scada/commit/2f0b88cf130fd86294cd29cf86dd43b8d6f252e2))

## [1.2.0](https://gitlab.com/moshin_han/scada/compare/v1.1.0...v1.2.0) (2020-06-24)


### Features

* add some words again ([184460f](https://gitlab.com/moshin_han/scada/commit/184460f2d4845e1c1de535263b98e25217f36892))


### Bug Fixes

* fix some bugs ([d8df2ba](https://gitlab.com/moshin_han/scada/commit/d8df2ba4cd88eaa7eb5271ac19c4468b17c051b0))

### [1.0.1](https://gitlab.com/moshin_han/scada/compare/v1.0.0...v1.0.1) (2020-06-24)


### Bug Fixes

* fix some bug ([d0c140d](https://gitlab.com/moshin_han/scada/commit/d0c140d165ac1565f9a64a26cc60050836cfa66d))

## 1.0.0 (2020-06-24)


### Features

* add some instroduction ([46949da](https://gitlab.com/moshin_han/scada/commit/46949da691db831a1b4c8bb0d3cde178a51e52a2))
